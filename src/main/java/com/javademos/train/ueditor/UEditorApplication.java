package com.javademos.train.ueditor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@EnableAutoConfiguration
@ServletComponentScan // 此配置用于自动扫描当前目录或者子目录下的Servlet
@SpringBootApplication
public class UEditorApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(UEditorApplication.class, args);
	}

}
