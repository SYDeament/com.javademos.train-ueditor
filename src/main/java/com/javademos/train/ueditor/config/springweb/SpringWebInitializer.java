package com.javademos.train.ueditor.config.springweb;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.javademos.train.ueditor.config.thymeleaf.ThymeleafConfiguration;

/**
 * @Description: [ 在启动荐中注册Thymeleaf配置项 ]
 *
 * @author 踩坑填坑
 * @Email: 120987555@qq.com
 * @Date: 2017年9月25日 下午1:52:16
 * 
 * @Version: V1.0
 */
public class SpringWebInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();

		// 注册Thymeleaf配置
		context.register(ThymeleafConfiguration.class);
		context.setServletContext(servletContext);

		// Spring MVC front controller
		Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(context));
		servlet.addMapping("/");
		servlet.setLoadOnStartup(1);
	}
}