package com.javademos.train.ueditor.config.thymeleaf;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;

/**
 * @Description: Thymeleaf模板相关配置
 *
 * @author 踩坑填坑
 * @Email: 120987555@qq.com
 * @Date: 2017年3月3日 下午7:31:48
 * 
 * @Version: V1.0
 *
 */
public class ThymeleafConfiguration {

	@Autowired
	private ITemplateResolver templateResolver;

	/**
	 * 
	 * @Description: 创建模板引擎<br/>
	 *               Thymeleaf shiro标签支持配置
	 *
	 * @Author: 踩坑填坑
	 * @Date: 2017年3月3日 下午7:32:22
	 * 
	 * @return
	 */
	@Bean
	public SpringTemplateEngine templateEngine() {
		Set<IDialect> additionalDialects = new HashSet<IDialect>();
		additionalDialects.add(new ShiroDialect());

		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		templateEngine.setAdditionalDialects(additionalDialects);

		return templateEngine;
	}

}