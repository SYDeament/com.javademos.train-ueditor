/**
 * 
 */
package com.javademos.train.ueditor.view.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baidu.ueditor.ActionEnter;
import com.javademos.train.ueditor.util.PathUtils;

/**
 * @Description: [ 百度UEditor文件上传Servlet ] <br/>
 *               之所以使用Servlet是可以直接绕过Spring Boot的控制
 *
 * @author 踩坑填坑
 * @Email: 120987555@qq.com
 * @Date: 2017年9月20日 上午9:53:33
 * 
 * @Version: V1.0
 */
@SuppressWarnings("serial")
@WebServlet(name = "UEditorUploaderServlet", urlPatterns = "/ueditorUpload.html")
public class UEditorUploaderServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setHeader("Content-Type", "text/html");

		PrintWriter out = response.getWriter();

		ServletContext application = this.getServletContext();
		String rootPath = application.getRealPath("/");

		// Spring-boot的path
		// rootPath = PathUtils.getClassPath(); // classpath路径
		rootPath = PathUtils.getStaticFolderPath();// classpath下的static路径

		String action = request.getParameter("action");

		String result = new ActionEnter(request, rootPath).exec();

		if (action != null //
				&& (action.equals("listfile") || action.equals("listimage"))) {
			rootPath = rootPath.replace("\\", "/");
			result = result.replaceAll(rootPath, "/");
		}

		out.write(result);
	}

}
