/**
 * 
 */
package com.javademos.train.ueditor.view.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.javademos.train.ueditor.util.RequestUtils;

/**
 * @Description: [ Demo控制器 ]
 *
 * @author 踩坑填坑
 * @Email: 120987555@qq.com
 * @Date: 2017年9月19日 下午4:30:58
 * 
 * @Version: V1.0
 */
@RestController
public class DemoController {

	@RequestMapping("/demo")
	public ModelAndView demo(Model model) {

		String ctx = RequestUtils.getCtx();

		model.addAttribute("ctx", ctx); // 上下文(请求协议+端口号+项目名称)
		System.out.println("传入到后台的上下文数据: " + ctx);

		// 带有双引号的数据
		String content = "带有双引的数据：<br/>图片路径:<img alt=\"图片\" src=\"/images/aaaa.jpg\">;";
		model.addAttribute("content", content);
		System.out.println("带有双引号的数据: " + content);

		return new ModelAndView("demo");
	}

}
