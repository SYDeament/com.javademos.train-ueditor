package com.javademos.train.ueditor.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.util.ClassUtils;

/**
 * @Description: [ 路径工具类 ]
 *
 * @author 踩坑填坑
 * @Email: 120987555@qq.com
 * @Date: 2017年7月25日 上午10:28:39
 * 
 * @Version: V1.0
 */
public class PathUtils {
	private static final Logger logger = LogManager.getLogger(PathUtils.class);

	/**
	 * @描述: 获取项目绝对路径(classpath)
	 *
	 * 
	 * @date: 2017年7月25日 上午10:31:21
	 *
	 * @return
	 */
	public static String getProjectAbsolutePath() {
		return ClassUtils.getDefaultClassLoader().getResource("").getPath();
	}

	/**
	 * @描述: 获取项目classpath路径
	 *
	 * 
	 * @date: 2017年7月25日 上午10:58:31
	 *
	 * @return
	 */
	public static String getClassPath() {
		return getProjectAbsolutePath();
	}

	/**
	 * @描述: 获取默认静态资源/resources/static/文件夹绝对路径
	 *
	 * 
	 * @date: 2017年7月25日 上午11:51:54
	 *
	 * @return
	 */
	public static String getStaticFolderPath() {
		return getClassPath() + "static/";
	}

	/**
	 * @描述: 获取指定文件路径的文件夹
	 *
	 * 
	 * @date: 2017年8月15日 上午9:12:51
	 *
	 * @param filePath
	 *            文件路径(classpath用相对路径,其它使用绝对路径)
	 * @param isClasspath
	 *            是否为 classpath相对路径
	 * @return
	 */
	public static String getFileDirectory(String filePath, boolean isClasspath) {
		// 文件完整路径
		String fullPath = filePath;
		if (isClasspath) {
			fullPath = getClassPath() + filePath;
		}
		// 替换右斜杠路径为左斜杠路径
		fullPath = StringUtils.replace(fullPath, "\\", "/");
		String directory = fullPath.substring(0, fullPath.lastIndexOf("/"));
		directory = StringUtils.replace(directory, "//", "/");
		if (!directory.endsWith("/")) {
			directory = directory + "/";
		}

		logger.info("文件所在路径:" + directory);
		return directory;
	}

}
